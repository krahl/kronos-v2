import 'package:analog_clock/models/timezone_db.dart';
import 'package:analog_clock/providers/saved_places.dart';
import 'package:flutter/material.dart';

class NewScreen extends StatelessWidget {
  final TimeZoneDB _timeZoneDB = TimeZoneDB();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add a new place'),
      ),
      body: ListView.separated(
        separatorBuilder: (context, index) => Divider(
          color: Theme.of(context).accentColor,
        ),
        itemCount: _timeZoneDB.itemsCount,
        itemBuilder: (ctx, i) => ListTile(
          title: Text(
            '${_timeZoneDB.itemByIndex(i).id}',
            style:
                TextStyle(color: Theme.of(context).textTheme.headline1.color),
          ),
          subtitle: Text(
            '${_timeZoneDB.itemByIndex(i).countryName} ${_timeZoneDB.itemByIndex(i).gmtOffset}',
            style:
                TextStyle(color: Theme.of(context).textTheme.headline1.color),
          ),
          onTap: () {
            SavedPlaces().addItem(_timeZoneDB.itemByIndex(i));
            Navigator.of(context).pop();
          },
        ),
      ),
    );
  }
}
