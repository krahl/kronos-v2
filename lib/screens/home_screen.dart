import 'package:analog_clock/utils/app_routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

import '../size_config.dart';
import '../providers/saved_places.dart';
import 'components/body.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    //precisa chamar isso no start da pagina
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: SvgPicture.asset(
            "assets/icons/Settings.svg",
            color: Theme.of(context).iconTheme.color,
          ),
          onPressed: () {},
        ),
        actions: [
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(10)),
            child: InkWell(
              onTap: () {
                Navigator.pushNamed(context, AppRoutes.ADD_TZ).then((value) {
                  setState(() {});
                });
              },
              child: Container(
                width: getProportionateScreenWidth(32),
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  shape: BoxShape.circle,
                ),
                child: Icon(
                  Icons.add,
                  color: Colors.white,
                ),
              ),
            ),
          )
        ],
      ),
      body: FutureBuilder(
        future: Provider.of<SavedPlaces>(context, listen: false).load(),
        builder: (ctx, snapshot) =>
            snapshot.connectionState == ConnectionState.waiting
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : Consumer<SavedPlaces>(
                    child: Center(
                      child: Text('Click on + to add our first place...'),
                    ),
                    builder: (ctx, savedPlaces, ch) =>
                        savedPlaces.itemsCount == 0
                            ? ch
                            : Body(savedPlaces.itemsWithOutDefault,
                                savedPlaces.getDefault),
                  ),
      ),
    );
  }
}
