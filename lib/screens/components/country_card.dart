import 'package:analog_clock/models/timezone_data.dart';
import 'package:analog_clock/providers/saved_places.dart';
import 'package:analog_clock/utils/app_routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:time_machine/time_machine.dart';

import '../../size_config.dart';

class ContryCard extends StatelessWidget {
  const ContryCard({
    Key key,
    @required this.timeZoneData,
    @required this.now,
    @required this.dateTimeZone,
  }) : super(key: key);

  final TimeZoneData timeZoneData;
  final Instant now;
  final DateTimeZone dateTimeZone;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        left: getProportionateScreenWidth(20),
      ),
      child: SizedBox(
        width: getProportionateScreenWidth(233),
        child: AspectRatio(
          aspectRatio: 1.32,
          child: Container(
            padding: EdgeInsets.all(
              getProportionateScreenWidth(20),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              border: Border.all(
                color: Theme.of(context).primaryIconTheme.color,
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      '${timeZoneData.countryName}, ${timeZoneData.countryCode}',
                      style: Theme.of(context).textTheme.headline4.copyWith(
                            fontSize: getProportionateScreenWidth(16),
                          ),
                    ),
                    InkWell(
                      child: Padding(
                        padding: EdgeInsets.only(left: 10),
                        child: Icon(
                          Icons.favorite_border,
                          size: getProportionateScreenWidth(16),
                        ),
                      ),
                      onTap: () async {
                        await SavedPlaces().changeDefault(timeZoneData.id);
                        Navigator.popAndPushNamed(context, AppRoutes.HOME);
                      },
                    )
                  ],
                ),
                SizedBox(
                  height: 5,
                ),
                Text(timeZoneData.gmtOffset),
                Spacer(),
                Row(
                  children: [
                    if (timeZoneData.iconSrc.trim().isNotEmpty)
                      SvgPicture.asset(
                        timeZoneData.iconSrc,
                        width: getProportionateScreenWidth(40),
                        color: Theme.of(context).accentIconTheme.color,
                      ),
                    Spacer(),
                    Text(
                      now.inZone(dateTimeZone).toString('hh:mm'),
                      style: Theme.of(context).textTheme.headline4,
                    ),
                    RotatedBox(
                      quarterTurns: 3,
                      child: Text(TimeOfDay.fromDateTime(now
                                      .inZone(dateTimeZone)
                                      .toDateTimeLocal())
                                  .period ==
                              DayPeriod.am
                          ? "AM"
                          : "PM"),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
