import 'dart:async';
import 'package:analog_clock/models/timezone_data.dart';
import 'package:analog_clock/models/timezone_db.dart';
import 'package:analog_clock/screens/components/clock.dart';
import 'package:analog_clock/screens/components/time_in_hour_and_minute.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:time_machine/time_machine.dart';
import 'country_card.dart';

// ignore: must_be_immutable
class Body extends StatefulWidget {
  List<TimeZoneData> _items = [];
  TimeZoneData _default;

  Body(
    this._items,
    this._default,
  );

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  DateTimeZoneProvider tzdb;
  Instant _now = Instant.now();
  TimeZoneDB _timeZoneDB = TimeZoneDB();

  Future<void> setup() async {
    await TimeMachine.initialize({
      'rootBundle': rootBundle,
    });
    tzdb = await DateTimeZoneProviders.tzdb;

    widget._items.forEach((item) async {
      item.dateTimeZone = await tzdb[item.id];
    });
    //Inicializa o Horario do Widget Padrao
    widget._default.dateTimeZone = await tzdb[widget._default.id];
  }

  @override
  void initState() {
    super.initState();

    if (!mounted) return;

    setup().then((_) {
      Timer.periodic(Duration(seconds: 1), (timer) {
        setState(() {
          _now = Instant.now();
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Column(
          children: [
            Text(
              '${widget._default.countryName} ${widget._default.gmtOffset}',
              style: Theme.of(context).textTheme.bodyText1,
            ),
            TimeInHourAndMinute(TimeOfDay.fromDateTime(
                _now.inZone(widget._default.dateTimeZone).toDateTimeLocal())),
            Spacer(),
            ClockComponent(
                _now.inZone(widget._default.dateTimeZone).toDateTimeLocal()),
            Spacer(),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: widget._items
                    .map(
                      (item) => ContryCard(
                        dateTimeZone: item.dateTimeZone,
                        now: _now,
                        timeZoneData: _timeZoneDB.timezoneByID(item.id),
                      ),
                    )
                    .toList(),
              ),
            )
          ],
        ),
      ),
    );
  }
}
