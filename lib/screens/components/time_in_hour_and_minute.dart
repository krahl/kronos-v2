import 'package:flutter/material.dart';
import '../../size_config.dart';

class TimeInHourAndMinute extends StatelessWidget {
  final TimeOfDay _timeOfDay;

  const TimeInHourAndMinute(this._timeOfDay);
  @override
  Widget build(BuildContext context) {
    final hour = _timeOfDay.hourOfPeriod < 10
        ? '0${_timeOfDay.hourOfPeriod}'
        : _timeOfDay.hourOfPeriod;
    final minute =
        _timeOfDay.minute < 10 ? '0${_timeOfDay.minute}' : _timeOfDay.minute;

    String _period = _timeOfDay.period == DayPeriod.am ? "AM" : "PM";
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "$hour:$minute",
          style: Theme.of(context).textTheme.headline1,
        ),
        SizedBox(
          width: 5,
        ),
        RotatedBox(
          quarterTurns: 3,
          child: Text(
            _period,
            style: TextStyle(
              fontSize: getProportionateScreenWidth(18),
            ),
          ),
        ),
      ],
    );
  }
}
