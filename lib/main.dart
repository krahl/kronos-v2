import 'package:analog_clock/models/my_theme_provider.dart';
import 'package:analog_clock/providers/saved_places.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'screens/home_screen.dart';
import 'screens/new_screen.dart';
import 'theme.dart';
import 'utils/app_routes.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => MyThemeModel(),
        ),
        ChangeNotifierProvider(
          create: (context) => SavedPlaces(),
        )
      ],
      child: Consumer<MyThemeModel>(
        builder: (context, theme, child) => MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Kronos',
          theme: themeData(context),
          darkTheme: darkThemeData(context),
          themeMode: theme.isLightTheme ? ThemeMode.light : ThemeMode.dark,
          home: HomeScreen(),
          routes: {
            //AppRoutes.HOME: (ctx) => HomeScreen(),
            AppRoutes.ADD_TZ: (ctx) => NewScreen(),
          },
        ),
      ),
    );
  }
}
