import 'package:time_machine/time_machine.dart';

class TimeZoneData {
  final String id;
  final String countryCode;
  final String countryName;
  final String gmtOffset;
  final String iconSrc;
  bool padrao = false;
  DateTimeZone dateTimeZone;

  TimeZoneData(
    this.id,
    this.countryCode,
    this.countryName,
    this.gmtOffset,
    this.iconSrc,
  );
}
