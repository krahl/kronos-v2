import 'package:analog_clock/utils/db_util.dart';
import 'package:flutter/material.dart';

class MyThemeModel extends ChangeNotifier {
  bool _isLightTheme = true;
  bool _loadedFromDB = false;
  Future<void> load() async {
    final dataList = await DbUtil.getData('settings');

    _isLightTheme = dataList[0]['theme_type'] == 'light';
    _loadedFromDB = true;
    notifyListeners();
  }

  Future<void> changeTheme() async {
    _isLightTheme = !_isLightTheme;
    DbUtil.insert('settings', {
      'id': '1',
      'theme_type': _isLightTheme ? 'light' : 'dark',
    });
    notifyListeners();
  }

  bool get isLightTheme {
    if (!_loadedFromDB) load();
    return _isLightTheme;
  }
}
