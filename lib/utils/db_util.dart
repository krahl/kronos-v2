import 'package:sqflite/sqflite.dart' as sql;
import 'package:path/path.dart' as path;

class DbUtil {
  static Future<sql.Database> database() async {
    final dbPath = await sql.getDatabasesPath();
    return sql.openDatabase(
      path.join(dbPath, 'places.db'),
      onCreate: (db, version) {
        db.execute('CREATE TABLE places (id TEXT PRIMARY KEY, padrao INTEGER)');
        return db.execute(
            'CREATE TABLE settings ( id TEXT PRIMARY KEY, theme_type TEXT)');
      },
      version: 1,
    );
  }

  static Future<void> insert(String table, Map<String, Object> data) async {
    final db = await DbUtil.database();
    final ret = await db.insert(
      table,
      data,
      conflictAlgorithm: sql.ConflictAlgorithm.replace,
    );
    print(ret);
  }

  static Future<List<Map<String, dynamic>>> getData(String table) async {
    final db = await DbUtil.database();
    return db.query(table);
  }

  static Future<int> getCount(String table) async {
    final db = await DbUtil.database();
    return sql.Sqflite.firstIntValue(
        await db.rawQuery('SELECT count(*) FROM $table'));
  }

  static Future<void> updateAll(
      String table, Map<String, dynamic> values) async {
    final db = await DbUtil.database();
    int ret = await db.update(table, values);
    print('retorno: $ret');
    return;
  }

  static Future<void> updateByID(
      String table, id, Map<String, dynamic> values) async {
    final db = await DbUtil.database();
    int ret = await db.update(table, values, where: "id = ?", whereArgs: [id]);
    print('retorno: $ret');
    return;
  }
}
