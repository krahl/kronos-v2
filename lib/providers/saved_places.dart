import 'package:analog_clock/models/timezone_data.dart';
import 'package:analog_clock/models/timezone_db.dart';
import 'package:flutter/material.dart';
import '../utils/db_util.dart';

class SavedPlaces with ChangeNotifier {
  List<TimeZoneData> _items = [];
  TimeZoneDB tzDB = TimeZoneDB();

  Future<void> load() async {
    final dataList = await DbUtil.getData('places');
    print(dataList.toString());

    _items = dataList.map((item) {
      TimeZoneData tmp = tzDB.timezoneByID(item['id']);
      tmp.padrao = item['padrao'] == 1 ? true : false;
      return tmp;
    }).toList();
    notifyListeners();
  }

  List<TimeZoneData> get items {
    return [..._items];
  }

  List<TimeZoneData> get itemsWithOutDefault {
    return [..._items.where((i) => !i.padrao)];
  }

  int get itemsCount {
    return _items.length;
  }

  TimeZoneData itemByIndex(int index) {
    return _items[index];
  }

  Future<void> addItem(TimeZoneData data) async {
    int count = await DbUtil.getCount('places');

    if (count < 1) data.padrao = true;

    _items.add(data);

    DbUtil.insert('places', {
      'id': data.id,
      'padrao': data.padrao ? 1 : 0,
    });

    notifyListeners();
  }

  TimeZoneData get getDefault {
    int qtd = _items.where((item) => item.padrao).length;
    return qtd > 0 ? items.where((item) => item.padrao).first : items.first;
  }

  Future<void> changeDefault(String id) async {
    await DbUtil.updateAll('places', {'padrao': 0});
    await DbUtil.updateByID('places', id, {'padrao': 1});
    notifyListeners();
  }
}
